window.onload = function () {
    const btn = document.getElementById("callAPI");
    btn.onclick = onClickButton;
};

function onClickButton() {
    let userTableBody = document.getElementById("userTableBody");
    let url = "https://jsonplaceholder.typicode.com/users";
    fetch(url)
        .then((response) => response.json())
        .then((data) => {
            for (let i = 0; i < 6; i++) {
                alert(data[i].name);
                let row = userTableBody.insertRow(-1);
                let cell1 = row.insertCell(0);
                let cell2 = row.insertCell(1);
                let cell3 = row.insertCell(2);
                let cell4 = row.insertCell(3);
                let cell5 = row.insertCell(4);
                let cell6 = row.insertCell(5);                
                cell1.innerHTML = data[i].id;
                cell2.innerHTML = data[i].name;
                cell3.innerHTML = data[i].username;
                cell4.innerHTML = data[i].email;
                cell5.innerHTML = data[i].phone;
                cell6.innerHTML = data[i].website;
            }
            // myDiv.innerHTML = JSON.stringify(data);
        });
}
