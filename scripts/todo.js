window.onload = function () {
    const btn = document.getElementById("callAPI");
    btn.onclick = onClickButton;
};

function onClickButton() {
    let myDiv = document.getElementById("myDiv");
    let todoId = document.getElementById("userId").value;
    let url = "http://jsonplaceholder.typicode.com/todos/" + todoId;
    fetch(url)
        .then((response) => response.json())
        .then((data) => {
            myDiv.innerHTML = JSON.stringify(data);
        });
}
