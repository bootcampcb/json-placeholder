window.onload = function () {
    const btn = document.getElementById("callAPI");
    btn.onclick = onClickButton;
};

function onClickButton() {
    let starWarsPeopleList = document.getElementById("charactersUL"); // document.querySelector('ul');
    let starWarsStarshipsList = document.getElementById("starShipsUL"); // document.querySelector('ul');

    fetch('https://swapi.dev/api/people')
        .then(function (response) {
            return response.json();
        })
        .then(function (json) {
            let people = json.results;

            for (p of people) {
                let listItem = document.createElement('li');
                listItem.innerHTML = '<p>' + p.name + ' : Eye color: ' + p.eye_color + '</p>';
                starWarsPeopleList.appendChild(listItem);
            }

        });
    fetch('https://swapi.dev/api/starships')
        .then(function (response) {
            return response.json();
        })
        .then(function (json) {
            let people = json.results;

            for (p of people) {
                let listItem = document.createElement('li');
                listItem.innerHTML = '<p>' + p.name + '</p>';
                starWarsStarshipsList.appendChild(listItem);
            }

        });

}
